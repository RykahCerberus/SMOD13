// PositionGrabber

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"#SMOD_PositionGrabber"
	"viewmodel"			"models/null.mdl"
	"playermodel"		"models/null.mdl"
	"anim_prefix"		"python"
	"bucket"			"tool"
	"bucket_position"	"6"

	"clip_size"			"1"
	"default_clip"		"1"
	"primary_ammo"		"pistol"
	"secondary_ammo"	"None"

	"weight"		"7"
	"rumble"		"2"
	"item_flags"		"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"empty"		"Weapon_Pistol.Empty"
		"single_shot"	"Buttons.snd17"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"WeaponIcons"
				"character"	"C"
		}
		"weapon_s"
		{	
				"font"		"WeaponIconsSelected"
				"character"	"C"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}